var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var size = 19
var array = create2DArray(size * 2, size, 0, true)

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      noStroke()
      fill(255)
      rect(
        windowWidth * 0.5 + (i - Math.floor(size * 0.5 * 2)) * (42 / 768) * boardSize * (17 / (size * 2.5)) * sin(frameCount * 0.01 + j * 0.2 + i * 0.2),
        windowHeight * 0.5 + (j - Math.floor(size * 0.5)) * (42 / 768) * boardSize * (17 / size),
        (42 / 768) * boardSize * (1 / size) * sin(frameCount * 0.01 + i * 0.2 + j * 0.2),
        (42 / 768) * boardSize * (17 / size) * (sin(frameCount * 0.01 + i * 0.22 + j * 0.23)))
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
